import random
import cv2
import numpy as np, matplotlib.pyplot as plt
import os, sys
import scipy.ndimage as ndimage
import zipfile



N = 11
W, H = 273, 273
DIR = "."
TARGET = "archive"

def sample_with_del(population, k):
	ret = random.sample(population, k)
	for e in ret:
		population.remove(e)
	return ret

def task(participants = 3, shared = 2, pair_shared=1, unique=2):
	if unique*participants + pair_shared*participants*(participants-1)//2 + shared > N:
		print("Task impossible : not enough tangrams")
		assert False
	selections = []
	tangrams = list(range(1, N+1))
	
	shared_choices = sample_with_del(tangrams, shared)
	pair_choices = {(i,j) : sample_with_del(tangrams, pair_shared) for i in range(participants-1) for j in range(i+1, participants)}
	unique_choices = []

	for participant in range(participants):
		unique_choice = sample_with_del(tangrams, unique)
		unique_choices.append(unique_choice)

		pair_choice = []
		for other in range(participants):
			if other != participant:
				i, j = min(other, participant), max(other, participant)
				pair_choice += pair_choices[i, j]
		
		participant_tangrams = unique_choice + pair_choice + shared_choices
		random.shuffle(participant_tangrams)
		
		selections.append(participant_tangrams)

	return selections, unique_choices, pair_choices, shared_choices


def construct_task_image(selection):

	bigH = H + 150
	bigW = W + 30

	n = len(selection)
	k = 1
	while k**2 < n:
		k+=1
	h, w = n//k, k
	if n%k != 0:
		h += 1
	
	image = 255*np.ones((h*bigH, w*bigW))
	
	for i, tangram in enumerate(selection):
		im = cv2.cvtColor(cv2.imread(f'{DIR}{os.sep}{tangram}.png'), cv2.COLOR_BGR2GRAY)
		im = im[:H,:W]
		x, y = i//w, i%w
		image[x*bigH : x*bigH + H, y*bigW : y*bigW + W] = im

	image_bordures = 255 * np.ones((30+h*bigH, 30+w*bigW))
	image_bordures[30:, 30:] = image
	
	def threshold(im, thres = 120):
		sup, inf = im>thres, im<=thres
		im[sup] = 255
		im[inf] = 0
		return im

	image = threshold(image_bordures, 88)

	font = cv2.FONT_HERSHEY_SIMPLEX
	for k in range(n):
		x, y = k//w, k%w
		cv2.putText(image, f'({k+1})', (y*bigW + W//2, x*bigH + H + 100), font, 1, (0,0,0), 4, cv2.LINE_AA)

	return image

def archive(path):
	selections, unique_choices, pair_choices, shared_choices = task()

	for i, selection in enumerate(selections):
		image = construct_task_image(selection)
		cv2.imwrite(f'participant_{i+1}.png', image)

	with open(f"Correction.txt", "w+") as file:
		for i, (selection, unique_choice) in enumerate(zip(selections, unique_choices)):
			unique = [str(k+1) for k in range(len(selection)) if selection[k] in unique_choice]
			shared = [str(k+1) for k in range(len(selection)) if selection[k] in shared_choices]
			participants = {}
			for p in range(len(selections)):
				if p != i:
					shared_with_p = pair_choices[min(p,i), max(p,i)]
					participants[p] = [str(k+1) for k in range(len(selection)) if selection[k] in shared_with_p]

			file.write(f"======== Participant {i+1} ========\n")
			file.write("Unique tangrams : " + ", ".join(unique) + '\n')
			file.write("Tangrams shared with everyone : " + ", ".join(shared) + '\n')
			for p in participants.keys():
				file.write(f"Tangram shared with participant {p+1} : " + ", ".join(participants[p]) + '\n')
			file.write('\n')

	files = []
	files.append('Correction.txt')
	for i in range(len(selections)):
			files.append(f'participant_{i+1}.png')

	with zipfile.ZipFile(path + ".zip", "w") as myzip:
		for file in files:
			myzip.write(file)

	print("Archive", path+".zip", "succesfully created")

	for file in files:
		os.remove(file)
	

if __name__ == "__main__":
	for k in range(1, 11):
		archive(f"{TARGET}_{k}")	
