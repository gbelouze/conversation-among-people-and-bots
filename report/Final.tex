\documentclass[9pt, english, twocolumn]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{appendix}
\usepackage{babel}
\usepackage{enumitem}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{geometry}
\usepackage{graphicx} % resizebox
\usepackage{subfig} % single column images in a 2-column environment
\usepackage{multicol}
\usepackage{array, multirow, makecell}
\usepackage[linktocpage=true]{hyperref}
\usepackage{dirtytalk} % quotations are easy with \say


 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }

%\geometry{hmargin = 2cm, vmargin = 2cm}

\title{Maintaining coherence in multiparty text-based conversations}
\author{
	Stierlin, Joana\\
	\and
	Pasalskaya, Lena\\
	\and
	Belouze, Gabriel\\}

\date{}

\begin{document}
\maketitle
\section*{Abstract}

In this paper, we investigate how participants of online multi-party written conversations handle the absence of paraverbal and non-verbal cues for turn-taking. We were interested in the behaviors used to maintain coherence in a group discussion, and more specifically strategies employed for responding to a specific message. We conducted an experiment aimed at generating turn-taking strategies in group text conversations. Generating data was necessary to have a controlled environment and to have a point of comparison between conversations, by making unique the goal of the conversation. After the annotation of the obtained data we concluded that timing drives the strategy employed to respond to messages. When timing between a message and the answer is long, the response tool is prefered. Calling out to somebody is used when the timing is shorter, and implicit messages are used for immediate response. Lexical cues matter when it comes to implicit answers. Our results seem to be valid for instant messaging but research remains to be done to investigate differences of strategies with other time scales.



\section{Introduction}

\paragraph{}Participants of verbal conversations use different types of cues to organize the conversational floor. Such cues can be verbal (e.g. naming a person), paraverbal (e.g. prosodic variations) or non-verbal, like using eye gaze or gesture []. 
Currently used chat platforms (Messenger, WhatsApp, Slack, WeChat, etc) do not allow users to use many of these cues  since they are lacking all sensorial cues. This raises a challenge  not only for dyadic conversation but also for group chats, where coherence is harder to maintain. In situations when two independent topics are raised at the same time, turn-taking cues are crucial to a felicitous conversation. 
\paragraph{}Thus, these online platforms adjust more and more to this problem and there are specific tools created to facilitate communication between members. For example, since 2017 Messenger allows a user to (i) mention a person, using ‘@’ (“tagging”), (ii) reply to one specific message, or (iii) react with emojis to a specific message [https://about.fb.com/news/2017/03/introducing-message-reactions-and-mentions-for-messenger/]. Through these tools, these messaging platforms try to recreate cues similar to paraverbal and non-verbal behaviors, that can help members  follow the conversation.
\paragraph{}These tools are created to facilitate organisation of the floor and coherence maintaining, which is a big issue for group text discussion.
In this paper, we investigate how participants maintain coherence and organize the floor in online text-based discussions. Coherence is partly maintained thanks to a good understanding of message addressivity (who is replying to whom) and thus it is important to study the strategic behaviors to respond to specific messages. 
We identified 4 different behaviors or strategies to respond to a specific message :
\begin{enumerate}
\item Using the response tool \\
\item Calling or tagging someone \\ 
\item Using the same lexical terms \\
\item Just answering with implicit receiver \\
\end{enumerate}

When do these different behaviors occur ? Depending on what parameters?
Our hypothesis is that the type of strategy used depends on:
Number of discussion topics at the same time
Timing between the first message and the answer
Number of messages between the original and the answer (related to the timing if we consider a constant messaging frequency)
In this study, we will concentrate on the analysis of timing of the messages, which seems to have the bigger influence on strategies according to the literature.




\section{Background}
\paragraph{}The ground for analysis of floor management and turn taking was laid in 1974 by Sacks \textit{and al.} \cite{Sacks1974} in an influential study (read in class), where they provide a list of 14 \say{grossly apparent facts} that a turn taking model must account for, and introduce the concept of Turn Relevant Places (TRPs) where turn taking can occur. Additionally, the model they define distinguish between three ranked strategies for turn taking :  speaker selection, when the current speaker selects the next speaker using names, eye gaze, gesture or other cues ; self selection, when another person self-selects as the next speaker ; and turn holding, when the current speaker self-selects to continue speaking.

\paragraph{}Coordination is more difficult for online chatting, because it is a problem to keep an understandable exchange. Berglund, T. Ö. \cite{Berglund2009DisruptedTA} focuses on how to make cohesive conversation. She found that message sequences were pretty clear. In the case messages are intertwined or ambiguous, five strategies were proposed to maintain coherence in online dyadic messaging: substitution, conjunction, repetition, anaphoric reference and feedback. Literature on text cohesion has a longer past and Halliday and Hasan (1976) \cite{Hasan} would have differed a bit from these categories. It seems that people adapted to this kind of communication by inventing new strategies. One type of strategy is about the verbal cues used, such as using explicit clarifications, similar lexical words (Gibson, W. (2009) \cite{Gibson}), repetition, reformulation (Lundin, M., \& Mäkitalo, Å. (2010) \cite{Lundin}), and addressivity (Markman, K. M. (2017) \cite{markman_2017}). Another type of strategy is to structure the conversation, by waiting for the end of the turn (Lundin, M., \& Mäkitalo, Å. (2010) \cite{Lundin}), posting short messages to keep the floor (González-Lloret, M. (2011) \cite{Lloret2011}) which give a temporal structure, and segmentation of the messages (Meredith, J. (2017) \cite{Meredith2017}), which give a space structure. This latter behavior has been used to create a new tool allowing the user to respond to a specific message.\\
Besides specific strategies, members of online conversations use some strategies that are shared by members of oral communication. Degand et al (2018) compares differences in turn-taking strategies between Instant Messaging and face-to-face communication, focusing on discourse markers. The hypothesis of the study is that utterance-final discourse markers are more essential for computer-mediated communication than for face-to-face communication. However, the results indicate that utterance-final discourse markers yield the turn in both Instant Messaging and face-to-face conversations, which suggests that conversation management is similar in both media.

\paragraph{} An interesting perspective on turn timing is given by Kalman \textit{and al.} \cite{Kalman}. They study timing between utterances in three distinct asynchronous CMC environments and observe a power law on response time. More specifically, if $\tau$ is the average inter response time, then three zones describe the response latency (RL) : 
\begin{itemize}[label = $\cdot$]
\item $RL<\tau$ : most responses fall in ther
\item $\tau < RL < 10\tau$ : some responses fall in there (about 25\%)
\item $10\tau < RL$ : little to no responses fall there
\end{itemize}
This can be particularly useful to predict the absence of a response, all the more that the model still holds true for user-specific average RL. Importantly, Kalman's study apply to asynchronous medium. Gill studied the inter response timing for quasi-synchronous environments, but found the task more challenging, as the response lattency is readily available. Indeed, for IMs, the time taken to type a message becomes comparable to that of the inter response time. Additionally, as mentioned previously, coherence can be broken, meaning that accounting for typing speed is not enough (since it is challenging to know which utterance is being responded to). This gives a particular perspective on Paulus recommendation that studies should not look to the text transcripts only, but rather to a screen recording of the conversations.

\section{Method}

We chose to generate our own data, in order to have a controlled environment with a restricted set of strategies, and more importantly to have a point of comparison between conversations, by standardizing the goal of the conversation. The experiment is a collaborative hard-to-solve task for a group of three people which requires from participants precise communications with each other, taking about 20 minutes to complete.


\paragraph{Online platforms}

The experiment was conducted in Messenger chats which allow users the use of specific text tools, namely $(i)$ @ (tagging) to identify of a person; $(ii)$ replying to a specific message with the "Respond" tool, thereafter referenced as \textit{Respond}, and $(iii)$ reacting to a specific message by using thumb-up, thumb-down or some other emojis. 

\paragraph{Design of the experiment}

Three participants, constituting one group, are given each a different page with 6 tangrams (Fig. \ref{experiment}). These pages contain different sets of pictures such that each participant has two unique tangrams, two tangrams that all participants have, one tangram that is shared only with one participant and one tangram that is shared only with the other participant. The task is to find out through discussion which tangrams are shared with whom and which tangrams are unique. The experiment takes approximately 20 minutes to complete (and we instruct the participants to be as fast as possible). The prompt given to the participants in English and in French can be found in the Appendix as well as the link to the obtained data.


\begin{figure*}[h]
\centering
\subfloat[][]{\includegraphics[width=.33\linewidth]{../images/participant_1.png}}
\subfloat[][]{\includegraphics[width=.33\linewidth]{../images/participant_2.png}}
\subfloat[][]{\includegraphics[width=.33\linewidth]{../images/participant_3.png}}
\caption{An example of three sets of images given to one group of participants}
\label{experiment}
\end{figure*}

\paragraph{Participants}

Participants were 15 French native speakers (5 male, 10 female, mean age 24,2, range age 18-49).  We collected data for 5 experiments (with for each, different sets of tangrams), resulting in about 100 minutes of conversation that we then annotated.

\paragraph{Annotation system}

We imported the data in excel table for the annotation (see appendix). We annotated for 3 features :
\begin{itemize}
\item The use of vocatives, annotating the name of the person being called
\item The use of the tool \textit{Respond}, annotating the explicitly referenced message
\item The implicitly referenced message, annotating for the previous message that elicited this response, or nothing if the message is spontaneous.
\end{itemize}
Additionaly, readily available from the data were the timestamps of the messages, down to the millisecond, the reactions with emojis, and the use of the tagging tool.


\paragraph{Statistical analysis}

The treatment of the data was made with Python (Gabriel did all the script since he has a computer science background). The source code can be found in the github.

\section{Results}


\subsection{Kalman's model}

\paragraph{} A natural interest which arises when discussing coherence is that of keeping track of the current topics at hand. Indeed at a given time, several \textit{sub-conversations} can be happening at once. Plurality can arise from two phenomena
\begin{itemize}
\item \textbf{Spontaneous creation}, whereby a message is sent without it being a response to any previous messages, thus beginning a new thread.
\item \textbf{Branching}, which occurs when several messages respond to a single message, thereby creating as many new threads.
\end{itemize}
Conversely, there must be phenomena that allow threads to disappear, without which keeping all relevant topics would quickly become untractable.

\paragraph{}The model described by Kalman \textit{and al} suggests such a phenomenon. They describe a pattern for asynchronous conversations (such as e-mail, or letter-exchanges), where, letting $\tau$ be the average response time in a given conversation,
\begin{enumerate}[label = (\roman*)]
\item Most responses come in a time less than $\tau$\\
\item An overwhelming majority of responses comes before $10\tau$
\end{enumerate}
We were interested in checking  this model against our data, which was generated by much more fast-paced conversations than that of Kalman. We could confirm that this result holds for each conversation taken separately (figure \ref{kalman_indiv}), and for the data as a whole (see figure \ref{kalman_whole}). This suggests that a very easy way of removing messages from the \textit{at-hand} topics is to simply letting them go after ten times the average response duration.

\begin{figure*}[h]
\centering
\subfloat[][]{\includegraphics[width=.45\linewidth]{../images/Kalman_indiv.png}}
\subfloat[][]{\includegraphics[width=.45\linewidth]{../images/Kalman_whole.png}}
\caption{Kalman-like distributions are found for instant-messaging conversations}
\label{kalman_indiv}
\label{kalman_whole}
\end{figure*}


\subsection{Determining the previous logical message}

\paragraph{} Keeping coherence amounts to determining the underlying structure of the messages. In this regard, given a message that is not spontaneous creation, a crucial challenge is being able to find which previous message ellicited this response. There are two facets to this : (a) reading a response, determine the previous logical message, but also, (b) writing a response, what strategies to use in order to ensure that \textit{the other participants} will be able to do (a).

\subsubsection{The \textit{Respond} tool}

\paragraph{} Our first analysis thus concerns the strategies that govern the use of the \textit{Respond} tool. In light of Kalman's model which highlights the importance of timing, we compared the use and absence of use of \textit{Respond} against the time between messages and their responses. Our expectation was that \textit{Respond} is used to disambiguate complex situations where it is necessary to mark the original message. Naturally, we expect this to be the case for long timing responses.

\paragraph{} We stripped two experiments from the data, which had respectively one and zero use-case of \textit{Respond}. We plotted the distribution of response timing, separating the explicit responses that made use of \textit{Respond} and the implicit ones. We noted that \textit{Respond} is much more often \textit{not} used, but also that there is a high variability of use rate of the tool across conversations.
As we hypothesised, \textit{Respond} is used to respond to generally older messages (see figure \ref{ImplicitExplicit}).

\begin{figure*}[h]
\centering
\subfloat[][]{\includegraphics[width=.75\linewidth]{../images/ImplicitExplicit}}
\caption{Response time for explicit and implicit responses}
\label{ImplicitExplicit}
\end{figure*}

\bibliographystyle{plain}
\bibliography{references}

\subsubsection{Implicit tagging}

\paragraph*{}The approach above is quite coarse : either a response is explicit when it uses \textit{Respond}, or it is not. We now try to fine grain our analysis and introduce a midpoint category. In our experiments, participants never used tagging, however we observed that participants sometimes call themselves in the vocative case, which we'll call \textbf{implicit tagging} (or more shortly tagging, since in the case of our data it is inambuguous). Intuitively, one could expect that implicitly tagging another participant could act as a midpoint between explicitly responding to a message, and not giving any cues at all.

\paragraph{}First, the role of implicit tagging should be described more carefully. Indeed, we found it to be actually ambiguous between previous speaker selection and next speaker selection. That is, sometimes implicit tagging is used to cue which participant is responded to, and sometimes to cue which participant one is expecting an answer from. We separated the tagging messages depending on $(i)$ whether they responded to a message which writer matched the tagged name, and $(ii)$ whether the person tagged responded to that particular message. We found that 36\% of tagging message fell in $(i)$ and 58\% in $(ii)$. The remaining 6\% weren’t responding to one of the tagged participant’s messages, nor were they responded to by the tagged participant. Looking at those particular cases by hand, we found that they were cases where the call was used to elicit an answer, but was ignored. These findings suggest that tagging can have two purposes : \textbf{mentioning} the author of the message being responded to, which we will call a mention, and eliciting an answer from the tagged person, which we will call an \textbf{elicitation}.

\paragraph{}With these categories defined, we extended our work in the previous section to include mentions. We could show that for conversations with enough use of mentions (i.e. more than 2 or 3), the mean response time were ordered as expected, that is
\begin{itemize}
\item Explicit responses generally come after a longer time
\item Mentions generally come after a slightly shorter time
\item Completely implicit responses generally come after an even shorter amount of time
\end{itemize}
This result should be mitigated for not being statistically robust, however it holds for the data taken as a whole (figure \ref{mention}. 

\paragraph{}Although we can observe this large-scale relationship, the role of mentions is more fine grained. In the following example, a mention is used to disambiguate a complex situation

\begin{table}[!h]
\hfill{}

\small
\texttt{
	\begin{tabular}{c c l}
39 & EC  &: parmi ceux assis\\
40 & EC  &: est ce que vous en avez sur les fesses\\
41 & LS  &: J'en ai un sur un pied qui fait du patinage ?\\
42 & LS  &: Ah\\
43 & LS  &: Oui\\
44 & EM  &: moi aussi Leyla\\
45 & EC &(responding to 41) : moi j’ai pas\\
	\end{tabular}
\hfill{}}
\label{conv_disambiguation}
\end{table}


(39-40) and (41) are two spontaneous utterances. At this point of conversation, the strategy used by the players is to discuss one specific kind of tangrams, so (39-40) and (41) are  mutually exclusive, it is a case of bump-in. This is acknowledged by (42). This bump-in is followed by (43) which answers to (39-40), implying that the conversation will now focus on (39-40). To answer to (41) instead, Etienne has to mention Leyla, in (44). Emma uses an even stronger disambiguation with \textit{Respond}, in (45).

\begin{figure*}[h]
\centering
\subfloat[][]{\includegraphics[width=.75\linewidth]{../images/Mentioning}}
\caption{Response time for explicit, mentioning and implicit responses}
\label{mention}
\end{figure*}

\subsection{Lexical analysis}

\paragraph{} A final strategy we observed was the choice of lexicon as a cue to ensure coherence. Consider the following example
\begin{table}[!h]
\hfill{}

\small
\texttt{
	\begin{tabular}{c c l}
55 & LS   	&: Bah moi j'ai mes résultat les grs\\
56 & LS   	&: Normalement\\
57 & EM 	&: Moi aussi\\
58 & EC     &: att j'ai 0 en commun juste avec toi etienne?\\
59 & EM 	&: si, le mec assis tout simplement\\
60 & EC     &: ok\\
61 & LS   	&: Tadaaa\\
62 & EM 	&: le dernier personnage que j’ai c’est \\
   & 		&\hphantom{:}  quelqu’un qui se penche en avant\\
63 & EC     &: alors moi aussi\\
	\end{tabular}
\hfill{}}
\label{conv_disambiguation}
\end{table}

Here, utterance (63) reuses the words from (57), which allows us to know which message it is responding to. This would otherwise be difficult, as (57) and (63) are separated by quite a number of messages. We can then formulate an hypothesis that lexical use can act as a cue to maintain coherence.

\paragraph{} To check our hypothesis, we computed a Levenshtein-like distance between pairs of message-response, which is a basic similarity measure, and we compared it against the lapse of time between the two messages. We observed a positive Pearson correlation ($r = 0.25$). This suggests that our expectations are good, that is for short time responses, the underlying structure is obvious enough, but for long time responses, some strategies must be used in order to ensure coherence. Hence a bias for lexicon similarity for longer time responses.


\subsection{Comparing strategies for the task experiment}

The participants of the experiment were instructed that their goal was to go as fast possible. We can compare the time it took them to complete the task to evaluate their respective strategies (see table \ref{experiment_lengths}).
\begin{table*}[!h]
\caption{Experiment length}
\label{experiment_lengths}
\begin{tabular}{|l |c| c |c |c |c |}
\hline
& Experiment 1 & Experiment 2 & Experiment 3 & Experiment 4 & Experiment 5\\
\hline
Duration of the task & 22 min & 18 min 30 & 32 min & 9 min 20 & 15 min\\
\hline
\end{tabular}
\end{table*}

We observed high variability of the employed strategy between experiments.\\
The most efficient group (Experiment 4) has a “compact” strategy: they described in one or 2 messages all their tangrams, and then did the comparison. Using a list to gather information is an efficient way to maintain coherence by avoiding all messages crossing. This data suggests that coherence is challenging enough that you are better off trying to have as little a conversation as possible. As written messages are persistent, they substitute memory by allowing you to read them at any time without any loss of information. The group who took the advantage of this specificity of text-based conversation performed better than the other groups who essentially replicated verbal conversations (although with more intricate sub conversations).\\
Experiment 5 is interesting because there is no use of tools except some implicit tagging. The initiation of the turn is made very explicit “J’y vais”; “Moi, j’ai […]”. There is one discussion thread at the same time. Everyone waits before sending a message, so there are very few so-called overlaps.\\
For the conversations that took the most time to finish the task (Experiments 1 and 3) there is much more tool use, and much more confusion. At the end of Experiment 1, the participants begin to use reactions, and our guess is that this reduces conversational floor pollution.

\paragraph{} Again however, the data is too restricted to emit robust conclusions about what the most efficient strategies are. In any case, the specifity of the task makes it unlikely for those strategies to be relevant in more common conversational instances.


\section{Limits}

\section{Future work}
Write a bot for the task of course.
Compare different platforms.
Repeat the experiment to add statistical robustness.

\bibliographystyle{plain}
\bibliography{references}

\section{*Appendix}
\appendix
\section{Task prompts}
\paragraph{English prompt}
\texttt{Each of you will receive a page with 6 pictures. These pages contain different sets of pictures, so that all pages are different. However some pictures are shared between you and the other participants. Among your 6 pictures, 2 are unique to only you and 2 are shared by everyone. Each of the last 2 pictures is shared with one participant, but not the other.
Your aim is for each of you to collaboratively find which pictures are shared between whom, and which pictures are unique.
Once you think you have all found how pictures are distributed, please write me personally which 2 pictures you think are unique to you, which 2 pictures you think are shared between everyone, and for each remaining pictures, with whom you think it is shared. For this, your pictures are numbered. However be careful : you cannot use these numbers while discussing, because pictures are randomly shuffled.
}\\
\paragraph{French prompt}
\texttt{Chacun.e de vous va recevoir 6 images, observez-les bien. Chaque lot d’images est différent. Par contre, vous possédez certaines images en commun avec les autres participants. Sur les 6 images, 2 images sont uniquement présentes dans votre lot. Vous possédez 2 images en commun avec tout le monde. Votre objectif est de trouver quelles images vous partagez avec qui, et quelles sont les images propres à votre lot (le plus rapidement possible).
Une fois que vous pensez avoir trouvé comment les images sont distribuées, écrivez-moi personnellement. Ecrivez quelles sont les deux images que vous pensez être uniques pour vous, quelles sont les 2 images que vous partagez avec seulement un participant, et quelles sont les deux images que vous partagez avec tous les participant.e.s. Pour vous aider à me donner vos réponses, vous pouvez vous aider de la numérotation. Attention cependant, vous ne pouvez pas vous aider des numéros pour discuter car les images sont mélangées dans chaque lot.
}

\section{Code for the analysis}
Available on \href{https://git.eleves.ens.fr/gbelouze/conversation-among-people-and-bots}{gitlab}
\end{document}