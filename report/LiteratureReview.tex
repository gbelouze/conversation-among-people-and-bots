\documentclass[english]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage{enumitem}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{geometry}
\usepackage{graphicx} % resizebox
\usepackage{multicol}
\usepackage{array, multirow, makecell}
\usepackage[linktocpage=true]{hyperref}
\usepackage{dirtytalk} % quotations are easy with \say


%\geometry{hmargin = 2cm, vmargin = 2cm}

\title{Turn-taking in multiparty text-based conversations}
\subtitle{Literature review}
\author{Joana Stierlin \& Lena Pasalskaya \& Gabriel Belouze}
\date{December 2020}

\begin{document}
\maketitle



\section{Theoretical considerations}

\subsection{Online text-based conversations}

\paragraph{Historical perspective} Currently used chat platforms (Messenger, WhatsApp, Slack, WeChat, etc) are modern instances of Instant Messaging (IM) applications. The first appearance of IMs, among which MSN Messenger was one of the first, dates back to 1996 with the emergence of ICQ. Subsequently, conversation analysis for online, real-time conversations is relatively new. In fact, the first review we could find of works on Computer Mediated Communication (CMC), which IM conversations are a part of, is as recent as 2016 with Paulus \textit{and al.} \cite{PaulusOnlineConvReview} \textit{ Applying conversation analysis methods to online talk: A literature review}, from which we found a great deal of references. The afore mentioned review distinguishes four main axis of study in the area of online talk. Two of these axis can be particularly relevant for us : online talk \textit{coherence}, and synchronicity. Around 30 of the studies mentioned in the review observed turn taking phenomena.

\paragraph{Coherence} Coherence in conversations is described by Donald Ellis as the \say{relations between utterances, sentences, or propositions that are pairwise and structured as sequential continuations} \cite{Ellis}. Crucially, in text based conversations, it is not uncommon that the usual adjacency pairs are interrupted between the first and the second utterance. This was first described as \say{disrupted turn adjacency} by Herring (\cite{Herring}).

\paragraph{Synchronous versus asynchronous} Synchronicity, or the lack there of, seems to be a prominent characteristic that shapes how conversations are conducted in a given medium. Some authors thus differientiate between written (e.g. telegrams) and spoken conversations (Crystal \cite{Crystal2002}). Many, however, argue for a continuum (most notably Baron \cite{Baron}). At one end of the spectrum we find face-to-face conversations (synchronous conversations), and at the other end we find letters by post, and to a lesser extent email and texts (asynchronous conversations). Our domain of interest, instant messaging systems, lie in between, bidded as \say{quasi-synchronous} by some authors since \textit{The Eyes of the Beholder: Understanding the Turn-Taking System in Quasi-Synchronous Computer-Mediated Communication} (Garcia and Jacobs \cite{GarciaJacobs}).

\subsection{Turn taking in conversation analysis}

The ground for analysis of floor management and turn taking was laid in 1974 by Sacks \textit{and al.} \cite{Sacks1974} in an influential study (read in class), where they provide a list of 14 \say{grossly apparent facts} that a turn taking model must account for, and introduce the concept of Turn Relevant Places (TRPs) where turn taking can occur. Additionally, the model they define distinguish between three ranked strategies for turn taking :  speaker selection, when the current speaker selects the next speaker using names, eye gaze, gesture or other cues ; self selection, when another person self-selects as the next speaker ; and turn holding, when the current speaker self-selects to continue speaking.

\subsection{Turn taking in text-based conversations}

\paragraph{Link with the Sacks model} Hastrdlová (2017) \cite{Harstrdlova} hypothesizes that the last two strategies from the Sacks model, namely self selection and turn holding, would be more common in text conversations. The first reason is that speakers can easily self-select and the second that there are no audio-visual cues during text conversations, preventing many speaker selection cues. However, results reveal the opposite: the first strategy is the most popular on Internet Relay Chat. In spite of these findings, authors generally agree that turn-taking rules from Sacks cannot be readily applied in the CMC medium. For instance, Gill \cite{Gill} notes that it is often the case that a speaker immediately holds the turn and self selects after an utterance, starting to type a following one. This violates the Sacks hierarchy which prescribes that speakers should hold the turn only if no other particpant has self selected. More generally, much more overlapping is allowed.

\paragraph{} The framework which seems to be prefered is the notion of conversational floor, proposed by Simpson, J. \cite{Simpson2005} and based on Edelsky, C. \cite{Edelsky1981WhosGT} who said that “The floor is defined as the acknowledged what’s-going-on within a psychological time/space” and added that “The “floor” is interactionally produced, in that speakers and hearers must work together at maintaining it”. Simpson argues that online dyadic conversation looks like multi-party onsite conversations (see also Cherny \cite{Cherny}). They developed a new framework to study coherence in online chat.

\paragraph{Coherence in written conversation} Coordination is more difficult for online chatting, because it is a problem to keep an understandable exchange. Berglund, T. Ö. \cite{Berglund2009DisruptedTA} focuses on how to make cohesive conversation. She found that message sequences were pretty clear. In the case messages are intertwined or ambiguous, five strategies were proposed to maintain coherence in online dyadic messaging: substitution, conjunction, repetition, anaphoric reference and feedback. Literature on text cohesion has a longer past and Halliday and Hasan (1976) \cite{Hasan} would have differed a bit from these categories. It seems that people adapted to this kind of communication by inventing new strategies. One type of strategy is about the verbal cues used, such as using explicit clarifications, similar lexical words (Gibson, W. (2009) \cite{Gibson}), repetition, reformulation (Lundin, M., \& Mäkitalo, Å. (2010) \cite{Lundin}), and addressivity (Markman, K. M. (2017) \cite{markman_2017}). Another type of strategy is to structure the conversation, by waiting for the end of the turn (Lundin, M., \& Mäkitalo, Å. (2010) \cite{Lundin}), posting short messages to keep the floor (González-Lloret, M. (2011) \cite{Lloret2011}) which give a temporal structure, and segmentation of the messages (Meredith, J. (2017) \cite{Meredith2017}), which give a space structure. This latter behavior has been used to create a new tool allowing the user to respond to a specific message.\\
Besides specific strategies, members of online conversations use some strategies that are shared by members of oral communication. Degand et al (2018) compares differences in turn-taking strategies between Instant Messaging and face-to-face communication, focusing on discourse markers. The hypothesis of the study is that utterance-final discourse markers are more essential for computer-mediated communication than for face-to-face communication. However, the results indicate that utterance-final discourse markers yield the turn in both Instant Messaging and face-to-face conversations, which suggests that conversation management is similar in both media.

\paragraph{} An interesting perspective on turn timing is given by Kalman \textit{and al.} \cite{Kalman}. They study timing between utterances in three distinct asynchronous CMC environments and observe a power law on response time. More specifically, if $\tau$ is the average inter response time, then three zones describe the response latency (RL) : 
\begin{itemize}[label = $\cdot$]
\item $RL<\tau$ : most responses fall in ther
\item $\tau < RL < 10\tau$ : some responses fall in there (about 25\%)
\item $10\tau < RL$ : little to no responses fall there
\end{itemize}
This can be particularly useful to predict the absence of a response, all the more that the model still holds true for user-specific average RL. Importantly, Kalman's study apply to asynchronous medium. Gill studied the inter response timing for quasi-synchronous environments, but found the task more challenging, as the response lattency is readily available. Indeed, for IMs, the time taken to type a message becomes comparable to that of the inter response time. Additionally, as mentioned previously, coherence can be broken, meaning that accounting for typing speed is not enough (since it is challenging to know which utterance is being responded to). This gives a particular perspective on Paulus recommendation that studies should not look to the text transcripts only, but rather to a screen recording of the conversations.

\paragraph{Multiparty turn-taking} As far as we have investigated, fewer studies focus on group text conversation, or tend not to separate dyadic and multi-party ones. Dyadic online conversation is already something quite complicated because several topics of discussion can be treated in parallel. This is indirectly explained by Simpson, J. (2005) \cite{Simpson2005} by the fact that online conversation shares more similarities with multi party conversation “dinner party conversation” than with dyadic spoken conversation theoretical structure.  Still, he studied group conversation. When it comes to group conversation, it is mostly studied in specific contexts, like online learning or professional meetings. Interestingly, cooperation and coordination in text conversations seem sometimes easier when there are 3 persons, as one can lead the discussion (Hastrdlová, Š. (2017) \cite{Harstrdlova}). Hastrdlová also shows that participants of a large group tend to subdivide into smaller discussion groups and membership of such groups is changed spontaneously.


\section{Practical considerations}

\paragraph{Chatbots for group conversation} The coupling of NLP breakthroughs in recent years (LSTMs, Word2Vec, Bert-like models), and the commercial applications has caused a focus in research and industry on the design of chat bots. Shockingly then, a recent review on chatbots by Seering \textit{and al.} \cite{SeeringDyadicOrMultiReview} \textit{Beyond Dyadic Interactions: Considering Chatbots as Community Members} reports little presence of bots in non-dyadic settings (13 out of 104), and even less in multiparty conversations (6 in total), with really only one mentionned paper by Candello \textit{and al.} \cite{Candello} tackling the challenge of real robot-users conversational interactions in a groupchat. \\ Candello \textit{and al.} implemented a dialogue system between one user and several bots for general conversations. In particular, they emphasised that \say{a big challenge on developing such multi-bot conversation system is turn-taking}. In a more detailed paper, De Bayser \textit{and al.} \cite{BayserArchitectureForMultiparty} review and develop state-of-the art techniques for implementing multiparty conversational chatbots. Crucially, they mention that \say{there is a lack of solutions for creating multiparty-aware chatbots} and that they \say{are not aware of any work dealing with modeling turn-taking in textual chats}. They provide advices on developping end-to-end chatbots which will be useful if we are to try our hands at it.

\paragraph{Implementing turn taking} As hinted above, close to no work mentions turn taking in text based conversations. Works that has been conducted for spoken conversations can be partially adapted to the textual modality. For instance, Traum and Akker \cite{TraumAkker} review algorithms to figure addressees from utterances, part of which can be readily used in text based conversations. The study by Bohus and Horviz \cite{bohus2010} can also be used to model a turn taking decision maker, but we need to abstract away from their work the use of non verbal cues.



\paragraph{Conclusion}
It is important to notice that this field is evolving very fast in terms of research and engineering, and there are lots of new strategies available since these last years that would be interesting to study. For instance, the online chat function enabling you to respond to a specific message by sliding the message to the right is something that appeared one year ago. It is important to keep in mind that some of these studies should be replicated integrating the news technologies and tools used to see if the results are still reproducible. Online chatting behaviors evolve with technology available, therefore research on this field remains to be done to adapt to the challenging fast evolution of modern online conversational tools. In particular, we found that most studies do not tackle the role of available tools such as tagging, message reactions, or explicit message responses, and we lack a comprehensive understanding of them. The use of the tool "X is typing" is briefly mentioned in Gill \cite{Gill}.

\section{Division of labour}
Joanna was mostly interested in the general framework of online chatting and text coherence, in the social science literature, and applied studies like strategies used while learning with online text conversation. Gabriel looked at both theoretical literature, especially on synchronicity and timing, and the practical literature, and compiled the review in latex, patching everyone's work into a single review. Lena added some detailed examples about described strategies of turn-taking in computer-mediated conversation from the theoretical literature. 



\bibliographystyle{plain}
\bibliography{references}



\end{document}