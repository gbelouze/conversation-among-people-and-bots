<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Contributors][contributors-shield]][contributors-url]
[![Issues][issues-shield]][issues-url]




<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://git.eleves.ens.fr/gbelouze/conversation-among-people-and-bots">
    <img src="images/Ens.png" alt="Logo" width="80" height="80">
  </a>

  <h1 align="center">Conversation among people and bots final project
</h1>

  <h3 align="center">
    Département d'Études cognitives, ENS 2020
    <br />
  </h3>


<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#results">Results</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About the project
This repository contains the source code for the final project of the [conversation among people and bots][conversation] course from the [Département d'Études Cognitives][dec] of ENS Paris.

We asked ourselves how multiparty online conversations maintained coherence, with a particular interest in the role of the new tools available on popular chat platform such as Messenger.

## Usage

## Results

## Contact

Gabriel Belouze - gabriel.belouze[at]ens.psl[dot]eu

<!-- MARKDOWN LINKS & IMAGES -->

[conversation]: https://docs.google.com/document/d/15dfVHFG2pIUEjytM7u0R3pYUy5N-zu9zP3QlJwwM7iU/edit
[dec]: https://cognition.ens.fr/

[contributors-shield]: https://img.shields.io/badge/contributors-1-green?style=for-the-badge
[contributors-url]: https://git.eleves.ens.fr/gbelouze/conversation-among-people-and-bots/-/project_members

[issues-shield]: https://img.shields.io/badge/issue-open-orange?style=for-the-badge
[issues-url]: https://git.eleves.ens.fr/gbelouze/conversation-among-people-and-bots/-/issues